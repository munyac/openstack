# How to run on production 

## Note Inventory files are different for each DC as below:
zwnerb/hosts-zwnerb-prod.yml - Baines DC
zwnerc/hosts-zwnerc-prod.yml - Telone DC
zwnerd/hosts-zwnerd-prod.yml - Pegasus DC

Generally speaking, this should just be normal ansible playbook execution. 

However, for those in a hurry (I would recommend you understand what you are doing) 
Gain access to a shell on the OOB Server (Jump Host generally) 

Warning - *this deploys everything* 

```ansible-playbook -i inventories/all-prod.yml push.yml ```

This will deploy the production task to the production inventory.

Note: These ansible scripts are idempotent, i.e. multiple executions will arrive at the same result.

## Zero Downtime (Odd / Even) Deployments 

To avoid impacting more than on failure domain at the same time, Zero Downtime Deployments are supported 

To execute a deployment in this fashion, the playbook is run in two parts

### ODD (more likely to be secondary) 
```ansible-playbook -i inventories/zwnerc/hosts-zwnerc-prod.yml push.yml --limit '~^.+?\d+?[13579]$'```

*N.B.* In the event that this deploy is a failure, STOP, review and correct *BEFORE* executing the EVEN, 
not following this simple instruction will incurr downtime.

### EVEN
```ansible-playbook -i inventories/zwnerc/hosts-zwnerc-prod.yml push.yml --limit '~^.+?\d+?[02468]$'```

# Monitoring

The ToR network is designed to be resilient, offerring n+1 fault tolerance in a single failure domain.

However, this resilience is only available if the network is not in a degraded state (as implied). 

Hence, monitoring is of critical importance to ensure readiness.

## Checklist

- MLAG status of each pair, (spines (odd,even), leaves (odd, even), etc). All devices that are assigned the "mlag" role, should be checked for status. 
- IPv4 BGP Peering, each neighbor should be online and propogating routes, A downed peer should be investigated.
- L2 EVPN BGP Peering, each neighbor should be  online  and propogating routes, A downed peer should be investigated.
- Hardware Status, Fans, Power Supplies, Daughter Boards, Lines Cards, etc, should be checked for alarms and tolerance variance. Any alarm indicats potential degraded failure domain, and the device should be swapped out (or resolved to an acceptable state as recommend by vendor)

## Frequency 

The frequency if these checks should be the shortest interval that the operators feel comfortable being in an unknown state, in an ideal world it would be in realtime (which is achieveable)

## Resolution

In the event that the on-site engineers are not able to resolve an identified issue within internal SLA, a support event should be raised at the Vendor. 


# How to run a lab 

To put the environment under simulation, checkout https://gitlab.com/cumulus-consulting/goldenturtle/cldemo2 (it is EOL but still works and is somehwat community maintained)

once you have this project, add the custom topology file from the ministro-cloud-network project that you wish to put under simulation. 

i.e. 

``` bash
~/w/m/i/cldemo2   *…  simulation  ./topology_converter.py ministro-lab-topology.dot -c

######################################
          Topology Converter
######################################
           originally written by Eric Pulvino

############
SUCCESS: Vagrantfile has been generated!
############

            18 devices under simulation.
                oob-mgmt-server
                oob-mgmt-switch
                DSBN-A-HRE-C
                lakota
                logan
                cdsp0001
                cdsp0002
                cdsp0003
                cdsp0004
                cdsp0005
                cdsp0006
                cdlt0001
                cdlt0002
                cdlt0003
                cdlt0004
                cdlt0005
                cdlt0006
                svci0001

            Requiring at least 9600 MBs of memory.

DONE! 
```

To begin spin up vagrant in the  `simulation` directory. (where the Vagrantfile that was generated from the topology_converter.py is located)


1. Bring up the OOB Mangement Network

``` vagrant up oob-mgmt-server oob-mgmt-switchi ``` 

2. Bring up the rest of the equipment under simulation.

``` vagrant up ```



3. Access the oob-mgmt-server  ``` vagrant ssh oob-mgmt-server ```

Note: you will only be able to access the simulation via the oob_mgmt_server

4. Git clone or copy the "ministro-cloud-network" to the oob-mgmt-server

5. Execute ansible playbook

``` bash
############################################################################
#
#         Out Of Band Management Server (oob-mgmt-server)
#
############################################################################
Last login: Fri Sep 17 08:49:01 2021 from 10.0.2.2
cumulus@oob-mgmt-server:~$ 
cumulus@oob-mgmt-server:~$ 
cumulus@oob-mgmt-server:~$ cd ministro-cloud-network/
cumulus@oob-mgmt-server:~/ministro-cloud-network$ ls
ansible.cfg  config  deploy-prod.yml  fetch.yml  group_vars  inventories  push.yml  README.md  roles  utils
cumulus@oob-mgmt-server:~/ministro-cloud-network$ ansible-playbook -i inventories/all-prod.yml push.yml 
[WARNING]: While constructing a mapping from /home/cumulus/ministro-cloud-network/inventories/all-prod.yml, line 3, column 9, found a duplicate dict key (switches). Using last defined value only.

PLAY [all] ***********************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************************************************************
[WARNING]: Platform linux on host cdlt0002 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0002]
[WARNING]: Platform linux on host cdsp0006 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0006]
[WARNING]: Platform linux on host cdlt0001 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0001]
[WARNING]: Platform linux on host cdlt0003 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0003]
[WARNING]: Platform linux on host cdsp0005 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0005]
[WARNING]: Platform linux on host cdlt0004 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0004]
[WARNING]: Platform linux on host cdlt0005 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0005]
[WARNING]: Platform linux on host cdlt0006 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0006]
[WARNING]: Platform linux on host cdsp0001 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0001]
[WARNING]: Platform linux on host cdsp0002 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0002]

TASK [Restore ports.conf] ********************************************************************************************************************************************************************************************************************
ok: [cdsp0005]
ok: [cdlt0003]
ok: [cdlt0001]
ok: [cdlt0002]
ok: [cdsp0006]
ok: [cdlt0004]
ok: [cdlt0005]
ok: [cdlt0006]
ok: [cdsp0001]
ok: [cdsp0002]

TASK [Restore Interface Configuration] *******************************************************************************************************************************************************************************************************
changed: [cdsp0006]
changed: [cdsp0005]
changed: [cdlt0002]
changed: [cdlt0003]
changed: [cdlt0001]
changed: [cdlt0004]
changed: [cdlt0005]
changed: [cdlt0006]
changed: [cdsp0002]
changed: [cdsp0001]

TASK [Restore FRR daemons file] **************************************************************************************************************************************************************************************************************
changed: [cdsp0005]
changed: [cdlt0003]
changed: [cdsp0006]
changed: [cdlt0002]
changed: [cdlt0001]
changed: [cdlt0005]
changed: [cdlt0004]
changed: [cdlt0006]
changed: [cdsp0001]
changed: [cdsp0002]

TASK [Restore frr.conf] **********************************************************************************************************************************************************************************************************************
changed: [cdlt0003]
changed: [cdsp0006]
changed: [cdsp0005]
changed: [cdlt0002]
changed: [cdlt0001]
changed: [cdlt0004]
changed: [cdlt0005]
changed: [cdlt0006]
changed: [cdsp0001]
changed: [cdsp0002]

RUNNING HANDLER [restart switchd] ************************************************************************************************************************************************************************************************************
changed: [cdlt0001]
changed: [cdsp0006]
changed: [cdsp0005]
changed: [cdlt0003]
changed: [cdlt0002]
changed: [cdlt0004]
changed: [cdlt0006]
changed: [cdlt0005]
changed: [cdsp0002]
changed: [cdsp0001]

RUNNING HANDLER [ifreload] *******************************************************************************************************************************************************************************************************************
changed: [cdsp0005]
changed: [cdsp0006]
changed: [cdlt0001]
changed: [cdlt0003]
changed: [cdlt0002]
changed: [cdlt0004]
changed: [cdlt0005]
changed: [cdsp0002]
changed: [cdlt0006]
changed: [cdsp0001]

RUNNING HANDLER [restart clag] ***************************************************************************************************************************************************************************************************************
changed: [cdsp0006]
changed: [cdlt0001]
changed: [cdlt0003]
changed: [cdlt0002]
changed: [cdsp0002]
changed: [cdlt0005]
changed: [cdlt0004]
changed: [cdlt0006]
changed: [cdsp0001]
changed: [cdsp0005]

RUNNING HANDLER [restart frr] ****************************************************************************************************************************************************************************************************************
changed: [cdsp0005]
changed: [cdsp0006]
changed: [cdlt0003]
changed: [cdlt0001]
changed: [cdlt0002]
changed: [cdlt0005]
changed: [cdlt0006]
changed: [cdsp0001]
changed: [cdlt0004]
changed: [cdsp0002]

PLAY RECAP ***********************************************************************************************************************************************************************************************************************************
cdlt0001                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0002                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0003                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0004                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0005                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0006                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0001                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0002                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0005                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0006                   : ok=9    changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   


```

Note: if ansible hangs at "Gatehring Facts", purge the ansible work dir, ``` rm -rf ~./ansible ``` and try again.

6. Reset the solution - with the reset.yml playbook

```
cumulus@oob-mgmt-server:~/ministro-cloud-network$ ansible-playbook -i inventories/all-prod.yml reset.yml 
[WARNING]: While constructing a mapping from /home/cumulus/ministro-cloud-network/inventories/all-prod.yml, line 3, column 9, found a duplicate dict key (switches). Using last defined value only.

PLAY [all] ***********************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************************************************************
[WARNING]: Platform linux on host cdsp0006 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0006]
[WARNING]: Platform linux on host cdsp0005 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0005]
[WARNING]: Platform linux on host cdlt0002 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0002]
[WARNING]: Platform linux on host cdlt0003 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0003]
[WARNING]: Platform linux on host cdlt0001 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0001]
[WARNING]: Platform linux on host cdlt0004 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0004]
[WARNING]: Platform linux on host cdlt0005 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0005]
[WARNING]: Platform linux on host cdlt0006 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdlt0006]
[WARNING]: Platform linux on host cdsp0002 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0002]
[WARNING]: Platform linux on host cdsp0001 is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [cdsp0001]

TASK [Reset config] **************************************************************************************************************************************************************************************************************************
changed: [cdsp0005]
changed: [cdsp0006]
changed: [cdlt0001]
changed: [cdlt0003]
changed: [cdlt0002]
changed: [cdlt0004]
changed: [cdlt0005]
changed: [cdlt0006]
changed: [cdsp0001]
changed: [cdsp0002]

RUNNING HANDLER [restart switchd] ************************************************************************************************************************************************************************************************************
changed: [cdlt0002]
changed: [cdsp0005]
changed: [cdlt0003]
changed: [cdsp0006]
changed: [cdlt0001]
changed: [cdlt0004]
changed: [cdlt0005]
changed: [cdlt0006]
changed: [cdsp0001]
changed: [cdsp0002]

RUNNING HANDLER [restart clag] ***************************************************************************************************************************************************************************************************************
changed: [cdsp0005]
changed: [cdsp0006]
changed: [cdlt0001]
changed: [cdlt0003]
changed: [cdlt0002]
changed: [cdlt0005]
changed: [cdlt0004]
changed: [cdlt0006]
changed: [cdsp0002]
changed: [cdsp0001]

RUNNING HANDLER [ifreload] *******************************************************************************************************************************************************************************************************************
changed: [cdsp0005]
changed: [cdsp0006]
changed: [cdlt0003]
changed: [cdlt0002]
changed: [cdlt0004]
changed: [cdlt0001]
changed: [cdlt0005]
changed: [cdlt0006]
changed: [cdsp0002]
changed: [cdsp0001]

PLAY RECAP ***********************************************************************************************************************************************************************************************************************************
cdlt0001                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0002                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0003                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0004                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0005                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdlt0006                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0001                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0002                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0005                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cdsp0006                   : ok=5    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

``` 

7. Tear down the simulation with ``` vagrant destory -f ```
