Role Name
=========

Configure VXLAN on VTEP's

Requirements
------------

BGP 
Loopbacks
MLAG

Role Variables
--------------

vxlan_vteps:
	$name: 
		vni: 100
		vid: 100 

Dependencies
------------

Example Playbook
----------------

    - hosts: zwner1
      roles:
         - add-vxlan

License
-------

BSD

Author Information
------------------

bgrobler@devshackintl.com
