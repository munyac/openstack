Add BGP 
=========

Configures BGP unnumbered on relevant devices.

Requirements
------------

add-loopback 
add-mlag 

Role Variables
--------------

router_asn 
router_id 

Dependencies
------------

router_id

Example Playbook
----------------

    - hosts: switches
      roles:
         - add-bgp

License
-------

BSD

Author Information
------------------

bgrobler@devshackintl.com
